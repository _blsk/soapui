# Containerized SoapUI
SoapUI in docker, based on openJDK

# ARG baseimage=<value> ARG tag=<value>
 defaults to openjdk:8-alpine for openJDK base image FROM baseimage:tag 
 Pass at build-time to the builder with the docker build command using --build-arg baseimage=<value> --build-arg tag=<value>

# ARG soapui_version=<value>
defaults to 5.4.0
Pass to set which SoapUI version to install (at build-time to the builder with the docker build command using --build-arg soapui_version=<value>

# some config ENVs 
will be needed if you start own / custom SoapUI project, pass the SOAPMOCKNAME, MOCK_HOME and PROJECTFILE ENVs

# Entrypoint 
default runs the sample test override with --entrypoint sh at runtime, if you want to peek into the container